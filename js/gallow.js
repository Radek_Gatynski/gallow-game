let passwordsTab = [
  'Practice makes perfect',
  'East or west home is best',
  'There is no place like home',
  'Love is blind',
  'Home is where the heart is',
  'Easy come easy go',
  'Time is money'
];


let password = passwordsTab[Math.floor(Math.random()*passwordsTab.length)];
password = password.toUpperCase();
let passwordLength = password.length;
let passwordWithoutSpace = 0;
let correctLetter = 0;
let hashPassword="";
let mistakes = 0;

for(let i=0;i<passwordLength;i++)
{
  if(password.charAt(i)==" ") 
    hashPassword += " ";
  else{
    passwordWithoutSpace++;
    hashPassword += "-";
  }
}

function write_password(){
  document.getElementById("board").innerHTML = hashPassword;
}

window.onload = start;

let alphabeth = new Array();
alphabeth = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','U','P','R','S','T','V','W','X','Y','Z'];

function start(){
  let div_content ="";
	for (i=0; i<alphabeth.length; i++)
	{
		let element = "char" + i;
		div_content = div_content + '<div class="letter" onclick="check('+i+')" id="'+element+'">'+alphabeth[i]+'</div>';
    if ((i+1) % 9 == 0) 
    div_content = div_content + '<div style="clear:both;"></div>';
	}
	document.getElementById("alphabet").innerHTML = div_content;
  write_password();
}

String.prototype.setChar = function(miejsce, znak){
  if(miejsce>this.length-1) 
    return this.toString();
  else 
    return this.substr(0, miejsce) + znak+this.substr(miejsce+1);
}


function check(nr){
let hit = false;
  for(i=0;i<passwordLength;i++){
    if(password.charAt(i)==alphabeth[nr]){
      hashPassword=hashPassword.setChar(i,alphabeth[nr]);
      hit = true;
      correctLetter++;
    }
  }
  if(passwordWithoutSpace == correctLetter){
    document.getElementById('chanceInfo').innerText = "You win";
    let btn =  document.getElementById('btn');
    btn.classList = "btn-restart btn-display";
    btn.addEventListener('click', function(){
      location.reload();
    })
  }
  let element = "char"+nr;
  if(hit==true){
    document.getElementById(element).style.background = "#003300";
    document.getElementById(element).style.color = "#00C000";
    document.getElementById(element).style.border = "#003300";
    write_password();
  }
  else{
      document.getElementById(element).style.background = "#003300";
      mistakes++;
      var obraz = "../assets/pic" + mistakes + ".jpg";
      document.getElementById("gallows").innerHTML='<img src="'+obraz+'"alt="" />';
      document.getElementById('chanceInfo').innerText = "Your chances: " + (9-mistakes);
      if(mistakes == 9){
        document.getElementById('chanceInfo').innerText = "You lost";
        let btn =  document.getElementById('btn');
        btn.classList = "btn-restart btn-display";
        btn.addEventListener('click', function(){
          location.reload();
        })
      }
  }
}
